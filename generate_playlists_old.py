#!/usr/bin/env python3
from time import sleep
import sys
import os

import playlists
import gsheet_interface

if __name__ == "__main__":
    print("Welcome to the interactive playlist generator! This utility allows you to create spotify or m3u (MPD) playlists based on a song.")
    sleep(0.5)
    options = (str(["spotify", "m3u"]))[1:-1]
    print("Please wait while we download Google Sheets data...", end = "")
    gsheets_data = gsheet_interface.process_data(gsheet_interface.setup_sheets())
    print("done\n")
    type = input(f"Which type of playlist would you like to generate? (Options: {options})")
    if type == "spotify":
        print("WIP")
    elif type == "m3u":
        master_path = input("What is the path of the master playlist, including the same number of Radiohead songs from your library as the google sheet used?")
        dest_path = input("Where would you like this new playlist saved? (Type 'mpd' for default mpd path in home dir) ")
        if dest_path == "mpd":
            dest_path = os.path.join(os.path.expanduser("~"),".config/mpd/playlists")
            print(dest_path)
        song_name = input("What song do you want the playlist to be based off of? ")
        print("Generating playlists..") 
        dest_name = input("What name would you like the playlist saved under? (Type 'default' to use the song name) ")
        if dest_name == "default":
            dest_name = song_name + ".m3u"

        sleep(0.1)
        
        master_dict = playlists.gen_dict_from_master_m3u(master_path, gsheets_data)
        playlists.gen_m3u(master_dict, song_name, os.path.join(dest_path,dest_name))


    else:
        print("Sorry, did not recognize that type of playlist.")

