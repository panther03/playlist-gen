import core

def gen_playlist(song: core.Song, songs: [core.Song], limit = 50):
    ''' Generates a playlist based on song_diff method. Most similar first. '''
    diff_to_song = lambda comp_song: core.song_diff(song, comp_song)
    return sorted(songs, key = diff_to_song)[:limit] 

def gen_m3u(db: dict, song_name: str, m3u_path: str):
    ''' Writes to an m3u file based on a Songs to path dictionary, a song name which maps to a Song object to generate the playlist off of, and the path that the m3u file should be saved.'''
    songs = list(db.keys())
    print(type(db))
    try:
        get_title = lambda x: x.title
        print(list(map(get_title, songs)))
        song = next(x for x in songs if x.title == song_name)
    except StopIteration as e:
        print("Song not found in master dictionary. Stopping.")
        return None
    value_of = lambda x: db[x]
    playlist = list(map(value_of, gen_playlist(song, songs)))
    try:
        with open(m3u_path, "w+") as f:
            for song_path in playlist:
                f.write(song_path + "\n")
    except Exception as e:
        print(f"Error writing to m3u: \n{e}")
    print("Successfully wrote to m3u.")
    return None

def gen_dict_from_master_m3u(master_path: str, master_list: [core.Song]):
    ''' Creates a dictionary from a path pointing to an m3u with all songs and a master list of Song objects. Must be equal length.'''
    try:
        with open(master_path, "r") as f:
            song_paths = str(f.read()).split('\n')[:-1]
            get_title = lambda x: x.title
            print(list(map(get_title, master_list)))
            print(song_paths)
            return dict(zip(master_list, song_paths))
    except Exception as e:
        print(f"Error reading from master m3u: \n{e}")
        return {}



    
