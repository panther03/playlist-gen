from oauth2client.service_account import ServiceAccountCredentials
import gspread
import core

def setup_sheets(main_sheet_name = "eloise"):
    '''Sets up Google sheet and returns a Spreadshete object for use with GSpread functions.'''
    scope = ['http://spreadsheets.google.com/feeds',
                    'https://www.googleapis.com/auth/drive']

    creds = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)
    client = gspread.authorize(creds)
    main_doc = client.open("ranking")
    main_sheet = main_doc.worksheet(main_sheet_name)
    return main_sheet

def sheet2list(main_sheet):
    ''' Returns sheet values as a list of lists.'''
    return main_sheet.get_all_values()

def sheet2csv(main_sheet, csv_name = "songs.csv", omit_header = True):
    ''' Writes sheet values to a csv, default name songs.csv. Option to toggle header omission.'''
    with open(csv_name, "w", newline="") as f:
        f.truncate()
        writer = csv.writer(f)
        data = main_sheet.get_all_values()
        if omit_header:
            writer.writerows(data[1:len(data)])
        else:
            writer.writerows(data)

def process_data(main_sheet):
    ''' Generate a list of Song objects. '''
    data = main_sheet.get_all_values()
    data = data[1:] # Skip header
    song_list = []
    for row in data:
        # Skip empty rows
        if row != ['','','','']:
            unknown = lambda x: 0 if x == "?" else x
            song_list.append(core.Song(title = row[0], sub = int(unknown(row[1])), emo = int(unknown(row[2])), int = int(unknown(row[3]))))
    return song_list

def gen_matlab_list(main_sheet):
    ''' 😉 '''
    return main_sheet.get_all_values().__str__()

