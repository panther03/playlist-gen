import math

SCALE = 42 # Scale value, do not touch
EXACT_MARGIN = SCALE/12 # Ask me for an explanation of why I decided this

def min_diff(x, y):
    '''Returns the circular difference between two point numbers based on SCALE variable.'''
    wrap_diff = SCALE-y+x if max(x,y) == y else SCALE-x+y
    return min(abs(y-x), abs(wrap_diff))

class Song:
    def __init__(self, title: str, sub: int, emo: int, int: int):
        '''Creates new Song instance. Attributes title, subject (sub), emotion (emo), intent (int).'''
        self.title = title
        self.sub = sub
        self.emo = emo
        self.int = int

    def __str__(self):
        string = f"Title: {self.title}\n"
        peaks = list(map(lambda x: SCALE*(x-1/6), [1.0/3.0,2.0/3.0,3.0/3.0]))

        if peaks[0] - EXACT_MARGIN <= self.sub < peaks[0] + EXACT_MARGIN:
            string += "Subject: Introspective\n"           
        elif peaks[0] + EXACT_MARGIN <= self.sub < peaks[1] - EXACT_MARGIN:
            string += "Subject: Introspective and Observational\n"
        elif peaks[1] - EXACT_MARGIN <= self.sub < peaks[1] + EXACT_MARGIN:
            string += "Subject: Observational\n"
        elif peaks[1] + EXACT_MARGIN <= self.sub < peaks[2] - EXACT_MARGIN:
            string += "Subject: Observational and Interpersonal\n"
        elif peaks[2] - EXACT_MARGIN <= self.sub < peaks[2] + EXACT_MARGIN:
            string += "Subject: Interpersonal\n"
        elif (peaks[2] + EXACT_MARGIN <= self.sub) or (self.sub < peaks[0] - EXACT_MARGIN):
            string += "Subject: Interpersonal and Introspective\n"
        else:
            string += "Subject: Not available\n"

        if peaks[0] - EXACT_MARGIN <= self.emo < peaks[0] + EXACT_MARGIN:
            string += "Emotion: Complacency (Gloom)\n"           
        elif peaks[0] + EXACT_MARGIN <= self.emo < peaks[1] - EXACT_MARGIN:
            string += "Emotion: Complacency (Gloom) and Fighting/Longing\n"
        elif peaks[1] - EXACT_MARGIN <= self.emo < peaks[1] + EXACT_MARGIN:
            string += "Emotion: Fighting/Longing\n"
        elif peaks[1] + EXACT_MARGIN <= self.emo < peaks[2] - EXACT_MARGIN:
            string += "Emotion: Fighting/Longing and Contentment (Acceptance)\n"
        elif peaks[2] - EXACT_MARGIN <= self.emo < peaks[2] + EXACT_MARGIN:
            string += "Emotion: Contentment (Acceptance)\n"
        elif (peaks[2] + EXACT_MARGIN <= self.emo) or (self.emo < peaks[0] - EXACT_MARGIN):
            string += "Emotion: Contentment (Acceptance) and Complacency (Gloom)\n"
        else:
            string += "Emotion: Not available\n"

        if peaks[0] - EXACT_MARGIN <= self.int < peaks[0] + EXACT_MARGIN:
            string += "Intent: Political"
        elif peaks[0] + EXACT_MARGIN <= self.int < peaks[1] - EXACT_MARGIN:
            string += "Intent: Political and Exsistential"
        elif peaks[1] - EXACT_MARGIN <= self.int < peaks[1] + EXACT_MARGIN:
            string += "Intent: Existential"
        elif peaks[1] + EXACT_MARGIN <= self.int < peaks[2] - EXACT_MARGIN:
            string += "Intent: Existential and Emotional"
        elif peaks[2] - EXACT_MARGIN <= self.int < peaks[2] + EXACT_MARGIN:
            string += "Intent: Emotional"
        elif (peaks[2] + EXACT_MARGIN <= self.int) or (self.int < peaks[0] - EXACT_MARGIN):
            string += "Intent: Political and Emotional"
        else:
            string += "Intent: Not available"
    
        return string

def song_diff(songA: Song, songB: Song):
    ''' Returns a looparound-respecting diff between two songs based on a 3D distance formula calculation.'''
    return math.sqrt(min_diff(songA.sub,songB.sub)**2 + min_diff(songA.emo, songB.emo)**2 + min_diff(songA.int, songB.int)**2)

if __name__ == "__main__":
    main_sheet = setup_sheets()
    songs = process_data(main_sheet)
    lucky = songs[-2]
    lplaylist = gen_playlist(lucky, songs)
    for song in lplaylist:
        print(song)
