#!/usr/bin/env python3
'''from time import sleep
import sys
import os

import playlists
import gsheet_interface

def 

if __name__ == "__main__":
    sleep(0.5)
    options = (str(["spotify", "m3u"]))[1:-1]
    print("Please wait while we download Google Sheets data...", end = "")
    gsheets_data = gsheet_interface.process_data(gsheet_interface.setup_sheets())
    print("done\n")
    type = input(f"Which type of playlist would you like to generate? (Options: {options})")
    if type == "spotify":
        print("WIP")
    elif type == "m3u":
        master_path = input("What is the path of the master playlist, including the same number of Radiohead songs from your library as the google sheet used?")
        dest_path = input("Where would you like this new playlist saved? (Type 'mpd' for default mpd path in home dir) ")
        if dest_path == "mpd":
            dest_path = os.path.join(os.path.expanduser("~"),".config/mpd/playlists")
            print(dest_path)
        song_name = input("What song do you want the playlist to be based off of? ")
        print("Generating playlists..") 
        dest_name = input("What name would you like the playlist saved under? (Type 'default' to use the song name) ")
        if dest_name == "default":
            dest_name = song_name + ".m3u"

        sleep(0.1)
        
        master_dict = playlists.gen_dict_from_master_m3u(master_path, gsheets_data)
        playlists.gen_m3u(master_dict, song_name, os.path.join(dest_path,dest_name))


    else:
        print("Sorry, did not recognize that type of playlist.")
'''
import click
import os.path
import sys
import gsheet_interface
import playlists

def get_sheets_data():
    click.echo("Please wait while we download Google Sheets data...", nl=False)
    try:
        gsheets_data = gsheet_interface.process_data(gsheet_interface.setup_sheets())
        click.secho("done", fg="green")
    except Exception as e:
        click.secho(f"failed. See error message below:\n{e}", fg="red")
        sys.exit(1)
    return gsheets_data

@click.command()
@click.option('--ptype', '-t')
@click.option('--song', '-s') 
@click.option('--output_m3u', '-om', \
        type = click.Path(file_okay = True, dir_okay = False), \
        help='Output file to save to. Default is default mpd playlists directory + song name.', \
        default="default")
@click.option('--master_m3u', '-im', \
        type = click.Path(file_okay = True, dir_okay= False), \
        help='Input master file to read from.') 

def generate_playlists(ptype, song, output_m3u, master_m3u):
    if song:
        gsheets_data = get_sheets_data() 
        if ptype == "m3u":
            if master_m3u:
                if output_m3u == "default":
                    output_m3u = os.path.join(os.path.expanduser('~/.config/mpd/playlists'), song.replace(" ","_") + ".m3u")
                else:
                    output_m3u = os.path.expanduser(output_m3u)
                master_m3u = os.path.expanduser(master_m3u)
                master_dict = playlists.gen_dict_from_master_m3u(master_m3u, gsheets_data)
                playlists.gen_m3u(master_dict, song, output_m3u)

            else:
                click.secho("Please provide a path to the master m3u file.")
                sys.exit(1)
        elif ptype == "spotify":
            print("WIP")
        else:
            click.secho("Please provide a valid playlist type. Can be one of spotify, or m3u", err=True, fg='red')
            sys.exit(1)
    else: 
        click.secho("Please provide a song name.", err=True, fg='red')
        sys.exit(1)
        
    
if __name__ == "__main__":
    generate_playlists()
